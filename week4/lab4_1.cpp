#include <iostream>
#include <fstream>
using namespace std;

int main() {
    string fileName;
    cout << "Введите имя файла: "; 
    cin >> fileName;

    ifstream file(fileName);
    if (file.is_open()) {
        string line;
        while (getline(file, line)) {
            cout << line << endl;
        }
        file.close();
    } else {
        cout << "Ошибка открытия файла: " << fileName << endl;
    }

    return 0;
}
